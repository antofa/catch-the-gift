import React, { Component } from 'react';
import pt from 'prop-types';
import cx from 'classnames';
import { get as prop } from 'lodash';

import './Gift.css';

class Gift extends Component {
    static propTypes = {
        hidden: pt.bool,
        name: pt.string,
        img: pt.string,
        numbers: pt.array,
        hightlightNumber: pt.number
    };

    static defaultProps = {
        hidden: true
    };

    /*constructor(props) {
        super(props);

        this.state = {
            hidden: true
        };
    }*/

    render() {
        const { name, img, hidden, numbers, hightlightNumber } = this.props;

        return (
            <div className='Gift-container'>
                <div>{name}</div>
                <img className='Gift-img' src={img} />
                <div className='Gift-numbers-container'>
                    {numbers.map(x => (
                        <div
                            className={`Gift-number ${hidden ? 'hidden' : ''} ${
                                !hidden && x === hightlightNumber ? 'highlight' : ''
                            }`}
                        >
                            {x}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default Gift;
