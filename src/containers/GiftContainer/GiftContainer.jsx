import React, { Component } from 'react';
import pt from 'prop-types';
import cx from 'classnames';
import _ from 'lodash';

import './GiftContainer.css';
import Gift from '../../components/Gift';

const GIFTS = [
    {
        name: 'Unexpected Gift',
        img: 'https://card.godsunchained.com/?id=2269&q=4'
    },
    {
        name: 'Winter Wanderlands',
        img: 'https://images.godsunchained.com/collectables/cardbacks/2005/2005_1024.webp'
    },
    {
        name: 'Unexpected Gift (trinket)',
        img: 'https://images.godsunchained.com/collectables/trinkets/1248--1080.webp'
    }
];

class GiftContainer extends Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);

        const numbers = _.shuffle(_.range(1, 100));

        this.state = {
            hightlightNumber: 0,
            hidden: true,
            numbers: [
                numbers.slice(0, 50),
                numbers.slice(50, 50 + 37),
                numbers.slice(50 + 37, 50 + 37 + 13)
            ],
            winChoice: _.sample(_.range(8))
        };
    }

    render() {
        const { hidden, numbers, winChoice, hightlightNumber } = this.state;

        return (
            <div>
                <button onClick={() => this.setState({ hidden: false })}>Show</button>
                <input
                    onChange={({ target: { value } }) =>
                        this.setState({ hightlightNumber: +value })
                    }
                    type='text'
                />
                <div className='Gifts-container'>
                    {GIFTS.map((x, index) => (
                        <Gift
                            {...x}
                            hidden={hidden}
                            numbers={numbers[index]}
                            hightlightNumber={hightlightNumber}
                        />
                    ))}
                </div>
                {!hidden && (
                    <div className='Gifts-pick-container'>
                        {_.range(8).map(index => (
                            <div>
                                <div>{index + 1}</div>
                                <div key={index} className='Gifts-pick'>
                                    {index === winChoice ? 'WIN' : 'LOSE'}
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}

export default GiftContainer;
